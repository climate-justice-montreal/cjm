---
title: "Protect the Great Lakes: shut down the line 5 oil pipeline"
lang: en
trans: lacs-2023-07
tags: action, line-5
---
Line 5 – a 70-year-old, dangerous, deteriorating crude oil pipeline – runs straight through the heart of the Great Lakes.

The pipeline, operated by Enbridge Inc., sits at the bottom of the Straits of Mackinac between Lake Michigan and Lake Huron. According to a University of Michigan study, Line 5 is located in the worst possible place for an oil spill. A rupture at the Straits of Mackinac could contaminate up to 1200 km of Great Lakes shoreline, devastating places like Manitoulin Island, Sauble Beach, and Tobermory. The pipeline has already leaked at least 33 times, spilling upwards of 4.5 million litres of oil since 1953. It needs to be shut down before it leaks again.

The Great Lakes are a source of drinking water for more than 40 million people in the US and Canada. They contain 21 per cent of the world’s surface fresh water and 84 per cent of North America’s fresh water. The Great Lakes are critically important and spiritually significant water bodies for the Anishinaabeg, the Haudenosaunee, and many Tribal Nations, First Nations, and Métis peoples. Line 5 threatens the waters, lands, and way of life for Indigenous peoples of the area and all peoples living in nearby shoreline communities.

The good news is, we do not need Line 5 in order to reliably meet our energy needs. But we do need political will and leadership to ensure Canada’s fresh water security. Canada and U.S. governments must prioritize the protection of the Great Lakes by working together with Tribal and Indigenous Nations to shut down Enbridge’s leaky Line 5 pipeline. We can continue to reliably heat our homes and drive our cars without Line 5 while transitioning to renewable energy.

We are urging the Canadian government to:
* Withdraw its use of the 1977 pipeline treaty in both the Michigan case and the Bad River Band of the Lake Superior Chippewas case
* Shut down Line 5 and require that Enbridge use existing, and readily available alternatives
* Respect the Anishinaabeg Tribes and First Nations of the Great Lakes exercising their inherent, treaty, and sovereign rights as the original stewards of the land by calling for the closure of Line 5
* Put the health and long-lasting preservation of the Great Lakes ahead of the short-term financial interests of the fossil fuel corporation, Enbridge Inc.
 
Take action today and [tell the federal government](https://act.environmentaldefence.ca/page/125608/action/1?ea.tracking.id=action){: rel="noopener" target="_blank"} to support the closure of Enbridge’s Line 5 pipeline and protect the Great Lakes.
