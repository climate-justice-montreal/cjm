---
title: "Hoodwinked in the Hothouse: Workshop on False Solutions to Climate Change"
lang: en
tags: education, hoodwinked
---
**Date:** Wednesday, December 6th, 2023  
**Time:** 2:30-4:30 pm  
**Location:** [John Molson Building](https://www.concordia.ca/maps/sgw-campus.html?building=MB){: rel="noopener" target="_blank"}  
  1450 Guy  
  Room 14.250  
**Cost:** Free  
**Wheelchair accessible:** Yes

![Hoodwinked flyer](/assets/images/hoodwinked.jpg)

Crafted around the zine, *Hoodwinked in the Hothouse: Resisting False Solutions to Climate Change*, Climate Justice Montreal presents a workshop that focuses on issues surrounding hydroelectricity, carbon capture, natural gas, as well as on the real solutions to the climate crisis!

> We see the pressing need to address the root causes of environmental and climate injustices by confronting four centuries of colonial-imperialism, ongoing patriarchal and white supremacist oppression, and today’s extreme neoliberal, globalized, industrial capitalist expansion. Hoodwinked demonstrates how climate change false solutions perpetuate, expand and reinforce these structures.
