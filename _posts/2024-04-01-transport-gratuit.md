---
title: "Assemblée Publique sur le Transport en Commun"
lang: fr
trans: free-public-transit-20240401
tags: éducation, action
---
![Affiche avec les détails de l'événement](/assets/images/transit_poster_FR.avif)

### Transport en commun gratuit, accessible et élargi : une perspective de justice climatique

[Svp RSVP!](https://forms.gle/3M64GBChffFtub3A9){: rel="noopener" target="_blank"}

Venez discuter de la possibilité d'un transport en commun gratuit, élargi et accessible à Montréal lors de cette assemblée publique. Les membres de JCM seront sur place pour discuter d'une perspective de justice climatique vers la construction d'une coalition inter-mouvements qui apportera l'un des piliers d'une ville juste pour toutes. Nous aurons également des expert-e-s et des personnes ayant une expérience vécue cruciale sur place pour mettre un terme à certains angles morts potentiels d'un mouvement de transport en commun : la justice pour les personnes handicapées, le potentiel radical d'avoir un transport en commun gratuit, comment le définancement des institutions oppressive peut aider à refinancer des services tels que le transport en commun, les réalités de la communauté autochtone urbaine et le risque de déplacement de populations lors de l'expansion des infrastructures axées sur le transport en commun. Les panélistes sont :
* Jason Prince - Prenons la Ville ! - Transport public gratuit
* Ted Rutland - Defund la Police - Refinancement du transport en commun
* Shirley - Indigenous Support Work Project - Réalités Autochtones urbaines
* Marie Turcotte - Ex Aequo - Accessibilité dans le transport en commun de Montréal
* Yasmine Hassen - Imagining an Otherwise - Déplacement axé sur le transport en commun

Nous sommes conscients que certains intérêts clés seront moins présents, tels que, mais sans s'y limiter : de bons emplois syndiqués face à un travail de bas niveau dû à l'uber-ification du transport en commun, le droit au logement comme mesure tout aussi importante pour avoir une ville juste et l'inclusion complète des réfugiés et des migrants grâce à une approche de statut pour tous. Ces sujets seront abordés, en collaboration avec les communautés directement concernées, lors de futurs événements et discussions.

Un repas, des services de garde d'enfants et la traduction de l'anglais vers le français seront disponibles sur place.

Organisé dans le cadre de la [semaine d'occupation et éducation populaire](https://mepacq.qc.ca/semaine-doccupation-et-deducation-populaire-du-15-au-18-avril-2024-appel-a-laction/){: rel="noopener" target="_blank"} du Mouvement d'éducation populaire et d'action communautaire du Québec ! 

Accessibilité : La Cité-des-Hospitalières dispose de nombreux escaliers, mais est accessible en fauteuil roulant, c'est à travers un monte-charge et un ascenseur qui mène à la salle Noviciat, située au deuxième étage. Veuillez communiquer avec nous à l'avance pour organiser l'entrée.

Affiche par Meredith Smallwood
