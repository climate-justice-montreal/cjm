---
title: "Beyond Fares: Building Climate-Resilient, People-Centered Transit Systems"
lang: en
trans: beyond-fares-20241122
---
[![Poster with event details](/assets/images/BeyondFares_Poster_final-1.avif){: .centered .half}](https://www.fb.com/share/9EGxbVPLdFhpXzuU/){: rel="noopener" target="_blank"}

What does a truly accessible transit system in Montreal look like?  

November 28th at 6 pm—join us at SHIFT for an evening dedicated to the future of accessible transit and climate action in Montreal! Climate Justice Montreal is hosting a panel that will bring community voices and scholars together to examine pathways toward fare-free, expanded public transit. The event will also celebrate the English launch of Against Car Culture: Transport for All! from Rage Climatique, a journal examining how capitalist and colonialist agendas fuel car dependency, rising transit costs, and decaying infrastructure. Together, let’s explore how we can build a just transit system that serves people and the planet.

Speakers:

Dr. Chun Wang is the Director of the Concordia Institute for Information Systems Engineering (CIISE); Mobile, Secure and Sharing Cities Cluster Co-Director for Next-Generation Cities Institute at Concordia University, Canada. He has published extensively in major intelligent transportation journals, covering topics in urban mobility, mobility-on-demand, and shared mobility systems. His expertise lies at the intersection of economic models, operations research, and artificial intelligence. Dr. Wang also serves as an associate editor of IET Collaborative Intelligent Manufacturing.

Climate Justice Montreal members will speak to the need for fare-free public transit for all with no restrictions, in a perspective of liberation, extensive public transit networks across the territory, built and renovated while avoiding gentrification and displacement, and accessible networks with an intersectional lens for centring mobility and diversity, which are the basis of their transit campaign.

Rage Climatique members will address some of the topics from its latest publication: Against Car Culture: Transport for All!, which includes decommodification of transport, the political topic of parking, Montreal being an ableist city and housing as a right in parallel to expanded transit access.
