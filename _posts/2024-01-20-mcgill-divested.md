---
title: "McGill Divested"
lang: en
trans: mcgill-divests-20240120
tags: education, action, metro
---
McGill finally divested, which spells an end to the Boycott Metro Campaign. Thanks to everyone who participated and congratulations on the victory.
