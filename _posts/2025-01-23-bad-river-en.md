---
title: "Free film & panel"
lang: en
trans: bad-river-20250123
tags: education, film
---
![Bad River poster](/assets/images/badriver-en.png)
Join us for this free event with popcorn + film and meet Bad River Band members Joe Bates and Gracie Waukechon...

Narrated by Academy Award nominee Edward Norton and Quannah Chasing Horse, Bad River chronicles the Wisconsin-based Bad River gang and its ongoing fight for sovereignty, a story that unfolds in revolutionary fashion through a series of shocking revelations.

...a David versus Goliath battle to save Lake Superior, America's greatest freshwater resource. As Bad River tribal elder Eldred Corbine declares: “We must protect it... die for it, if we must."

The film will be shown in English, with French subtitles, and the panel discussion will be in English, accompanied by a French translator.

[https://www.badriverfilm.com/](https://www.badriverfilm.com/){: rel="noopener" target="_blank"}

...a story of defiance... 
