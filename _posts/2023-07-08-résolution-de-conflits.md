---
title: "Travail d'équipe : résolution de conflits et prise de décision 🌿 Formations estivales"
lang: fr
trans: conflit-2023-07
tags: action
---
Date : 18 juillet 2023  
Heure : 3-5 pm  
Lieu : 1864 Rue Préfontaine, Montréal, QC H1W 2P1, Canada  

Cette formation divisée en deux segments abordera d’abord la résolution de conflits au sein d’un groupe, puis traitera de méthodes de prise de décision par consensus et consentement. Pourquoi lier ces deux thèmes au sein d’une même formation ? Bien qu’ils mobilisent des mécanismes différents, les façons de prendre des décisions peuvent générer énormément de conflits et inversement, les conflits se complexifient dans les contextes où les mécanismes de prise de décision sont flous.
