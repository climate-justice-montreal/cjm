---
title: Beehive Collective - Book Launch in Montreal
lang: en
trans: beehive-2023
tags: education
---
Date: July 14th, 2023  
Time: 4-6 pm  
Location: Parc Alphonse-Télésphore-Lépine  

### "The True Cost of Coal" Book launch
An event for youth, educators, parents, and the progressive community.

Join us at 4pm on Friday, July 14th as we gather at Parc Alphonse-Télesphore-Lépine to welcome members of the Beehive Collective to Montreal as they tour through with their children's book: "The True Cost of Coal". We will be hosting travelling artists so the event will take place in English; an introduction will be made in French. The Q&A will be bilingual.
