---
title: "McGill a cessé certains investissements toxiques"
lang: fr
trans: mcgill-divests-20240120
tags: education, action, metro
---
Enfin, McGill a promis de cesser ses investissements dans les combustibles fossiles. Ce fait marque la fin du boycott de l'épicerie Metro. Merci à tout le monde qui a participé et félicitations pour la victoire!
