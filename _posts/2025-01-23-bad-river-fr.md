---
title: "Film gratuit & panel"
lang: fr
trans: bad-river-20250123
tags: education, film
---
![Bad River poster](/assets/images/badriver-fr.png)

Rejoignez-nous pour cet événement gratuit avec popcorn + film et rencontrez les membres de Bad River Band Joe Bates et Gracie Waukechon...

Narré par Edward Norton, candidat à l'Academy Award et Quannah Chasing Horse, Bad River est une chronique de la bande de Bad River, basée dans le Wisconsin, et de son combat permanent pour la souveraineté, une histoire qui se déroule de manière révolutionnaire à travers une série de révélations choquantes.

...un combat de David contre Goliath pour sauver le lac Supérieur, la plus grande ressource d'eau douce d'Amérique. Comme le déclare Eldred Corbine, un ancien de la communauté de Bad River : « Nous devons le protéger... mourir pour lui, s'il le faut ».

Le film sera projeté en anglais, sous-titré en français, et la table ronde sera accompagnée d'un traducteur français.

[https://www.badriverfilm.com/](https://www.badriverfilm.com/){: rel="noopener" target="_blank"}

...une histoire de défi...
