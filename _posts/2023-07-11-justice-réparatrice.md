---
title: "Justice réparatrice : Voies intersectionnelles pour les soins communautaires"
lang: fr
trans: justice-2023-07
tags: autochtone
---
Date : 24 juillet 2023  
Heure : 6:30-8 pm  
Lieu : Événement en ligne  

Dans cet atelier, les participants apprennent à s'engager dans des pratiques de guérison qui remettent en question les systèmes coloniaux et capitalistes dominants. En tissant ensemble l'environnementalisme intersectionnel, la justice envers les personnes handicapées et les enseignements issus des approches noires et autochtones de la justice et de la guérison, cet atelier permet aux participants de comprendre les principes et pratiques de la justice réparatrice intersectionnelle, qui vont au-delà de l'hyper-individualisme et des tendances en matière de bien-être, et qui embrassent les soins communautaires et la guérison.
