---
title: "Protégeons les grands lacs : fermons le pipeline canalisation 5"
lang: fr
trans: lacs-2023-07
tags: action, line-5
---
La canalisation 5 est un pipeline bâti il y a 70 ans, qui est dangereux et se détériore. Il passe directement à travers les Grands lacs.

Le pipeline, qui appartient à Enbridge Inc., se situe au fond du détroit de Mackinack entre le lac Michigan et le lac Huron. D’après une étude de l’Université du Michigan, l’emplacement de la canalisation 5 est le pire en ce qui concerne les risques de déversement accidentel d’hydrocarbures. Une rupture du pipeline au détroit de Mackinack pourrait contaminer les rivages des Grands lacs sur une distance pouvant atteindre 1200 km et entraîner la dévastation de plusieurs lieux, y compris l’Île Manitoulin, Sauble Beach et Tobermory. Le pipeline a déjà eu au moins 33 fuites, et a déversé jusqu’à 4,5 millions de litres de pétrole depuis 1953. Il faut fermer ce pipeline avant qu’il n’y ait d’autres fuites.

Les Grands lacs sont une source d’eau potable pour plus de 40 millions de personnes aux États-Unis et au Canada. Ils constituent 21% de l’eau douce dans le monde entier, et 84% de l’eau douce dans toute l’Amérique du Nord. Pour les Anishinabeeg, les Haudenosaunee et un grand nombre d’autres nations et tribus autochtones, Premières Nations et peuples métis, les Grands lacs ont une énorme importance et signification spirituelle. La canalisation 5 menace les cours d’eau, les terres, et le mode de vie des peuples autochtones de la région, ainsi que de tous les peuples qui habitent près des rives environnantes.

La bonne nouvelle, c’est que nous n’avons pas besoin de la canalisation 5 pour subvenir à nos besoins en matière d’énergie. Cependant, nous devons mobiliser tous nos efforts en matière de politique et de leadership pour assurer la sécurité des ressources d’eau douce du Canada. Les gouvernements du Canada et des États-Unis doivent placer en priorité la protection des Grands lacs en collaborant avec les Nations et les peuples autochtones afin de faire fermer ce pipeline défaillant d’Enbridge qu’est la canalisation 5. Nous pouvons continuer à chauffer nos maisons et alimenter nos voitures sans la canalisation 5, tout en faisant la transition vers l’énergie renouvelable.

Nous exhortons le gouvernement canadien de :
* Cesser d’invoquer le Traité de 1977 sur les pipelines pour ignorer l’ordre du Michigan et l’ordre de la bande Bad River of Lake Superior Chippewa
* Fermer la canalisation 5 et exiger qu’Enbridge utilise des solutions de rechange qui s’offrent à elle
* Respecter les tribus Anishinaabeg et les Premières Nations des Grands lacs qui exercent leurs droits de traité et leurs droits souverains inhérents en tant que premiers protecteurs du territoire, et ordonner la fermeture de la canalisation 5.
* Faire passer en premier la santé et la préservation des Grands lacs avant les intérêts financiers à court terme de l’entreprise de combustibles fossiles Enbridge Inc.
Prenez action dès maintenant[en signeant cette pétition](https://act.environmentaldefence.ca/page/125608/action/1?ea.tracking.id=action){: rel="noopener" target="_blank"} et dites à votre gouvernement de soutenir la fermeture de la canalisation 5 d’Enbridge pour protéger les Grands lacs.


