---
title: "Teamwork: Conflict Resolution and Decision-Making 🌿 Summer Training Sessions"
lang: en
trans: conflit-2023-07
tags: action
---
Date: July 18th, 2023  
Time: 3-5 pm  
Location: 1864 Rue Préfontaine, Montréal, QC H1W 2P1, Canada  

This training, divided into two segments, will first address conflict resolution within a group, and then discuss methods of consensus and consent-based decision-making. Why combine these two themes in one training? Although they involve different mechanisms, the ways decisions are made can generate significant conflicts, and conversely, conflicts can become more complex in contexts where decision-making mechanisms are unclear.
