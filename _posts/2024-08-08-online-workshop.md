---
title: "Online workshop"
lang: en
trans: 202408-workshop
tags: education, action
---
Come learn how false solutions are being implemented and how we can resist them!  
Wednesday, August 14th, 7pm  
Register at [this link](https://linktr.ee/climatejusticemtl){: rel="noopener" target="_blank"}

[![Online workshop poster](/assets/images/2024-08-Resist false solutions Aug14th.avif)](https://linktr.ee/climatejusticemtl){: rel="noopener" target="_blank"}
