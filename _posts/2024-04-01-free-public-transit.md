---
title: "Transit Town Hall"
lang: en
trans: free-public-transit-20240401
tags: education, action
---
![Poster with event details](/assets/images/transit_poster_ENG.avif)

### Fare-free, Accessible and Expanded Public Transit: A Climate Justice Perspective

[Please RSVP](https://forms.gle/Kmk99ZXKa8MWtHfy8){: rel="noopener" target="_blank"}

Come discuss the possibility of fare-free, expanded and accessible public transit in Montreal at this town hall. Members of CJM will be on hand to discuss a climate justice perspective to building an inclusive path to building a cross-movement coalition that will bring about one of the pillars of a just city for all. We will also have experts and folks with crucial lived experience on hand to shine a light on some potential blind spots of a public transit movement: disability justice, the radical potential of having free transit, how defunding oppressive institutions can help refund public services, the realities of the urban Indigenous community and the risk of displacement when expanding transit-oriented infrastructure. The panelists are:
* Jason Prince - Prenons la Ville! - Free Public Transport
* Ted Rutland - Defund la Police - Refunding for Transit
* Shirley - Indigenous Support Work Project - Urban Indigenous realities
* Marie Turcotte - Ex Aequo - Accessibility in Montreal Transit
* Yas - Imagining an Otherwise - Transit Oriented Displacement

We are aware that some key interest will be less present, such as, but not limited to: good, unionized jobs in the face of low-grade work through Uber-ification of transit, right to housing as an equally important measure to having a just city and full inclusion of refugees and migrants through a status for all approach. These will be addressed, while working with directly affected communities, in future events and discussions.

Food, child care and English to French translation will be available on site.

Accessibility: The Cité-des-Hospitalières has many sets of stairs, but is wheelchair accessible, this is through a cargo loader and an elevator that leads to the salle Noviciat, located on the second floor. Please communicate with us beforehand to arrange entry. 

Poster by Meredith Smallwood
