---
title: "Healing Justice: Intersectional Pathways for Community Care"
lang: en
trans: justice-2023-07
tags: indigenous
---
Date: July 24th, 2023  
Time: 6:30-8 pm  
Location: Online Event  

In this workshop, participants learn how to engage in practices of healing that challenge dominant colonial and capitalist systems. Weaving together intersectional environmentalism, disability justice, and teachings from Black and Indigenous approaches to justice and healing, this workshop equips participants with a strong understanding of intersectional healing justice principles and practices that go beyond hyper-individualism and wellness trends and that embrace community care and healing.
