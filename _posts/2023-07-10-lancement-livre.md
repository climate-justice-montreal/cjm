---
title: Beehive Collective - Lancement du livre à Montréal
lang: fr
trans: beehive-2023
tags: éducation
---
Date : 14 juillet 2023  
Heure : 4-6 pm  
Lieu : Parc Alphonse-Télésphore-Lépine  

### Lancement du livre "Le véritable coût du charbon"
 
Un événement pour les jeunes, les éducateurs, les parents et la communauté progressiste. Nous accueillerons des artistes itinérants et l'événement se déroulera donc en anglais ; une introduction sera faite en français. Les questions et réponses seront bilingues.
