<hr style="height:10px;visibility:hidden;margin:0"/>

## {{ site.title-fr }} 

{% for each in site.data.navigation.footer-fr %}[{{ each.title }}]({{ each.link }}){% unless forloop.last == true %} &ensp;{% endunless %}{% endfor %}

Suivez-nous sur [Facebook](https://www.facebook.com/ClimateJusticeMontreal/){: rel="noopener" target="_blank"} ou [Instagram](https://www.instagram.com/justiceclimatiquemtl/){: rel="noopener" target="_blank"}


<br>

[![License CC]({{ site_link }}/assets/images/cc_logo.png){: img_center}](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr){: rel="noopener" target="_blank"}

© Copyright 2017-{{ 'now' | date: "%Y" }}, Justice climatique Montréal.
