<hr style="height:10px;visibility:hidden;margin:0"/>

## {{ site.title }}

{% for each in site.data.navigation.footer-en %}[{{ each.title }}]({{ each.link }}){% unless forloop.last == true %} &ensp;{% endunless %}{% endfor %}

Follow us on [Facebook](https://www.facebook.com/ClimateJusticeMontreal/){: rel="noopener" target="_blank"} or [Instagram](https://www.instagram.com/justiceclimatiquemtl/){: rel="noopener" target="_blank"}

<br>

[![Creative Commons License]({{ site_link }}/assets/images/cc_logo.png){: img_center}](https://creativecommons.org/licenses/by-nc-sa/4.0/){: rel="noopener" target="_blank"}

© Copyright 2017-{{ 'now' | date: "%Y" }}, Climate Justice Montreal.
