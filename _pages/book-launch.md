---
title: "Book Launch"
lang: en
trans: book-launch
permalink: /campaigns/book-launch
---
On April 20th, Climate Justice Montreal co-organized a book launch for The End of This World - Climate Justice in so-called Canada. Co-organized with Research for the Frontlines and supported by The Climate Justice Organizing Hub and Office of Sustainability - Concordia University, the event was held at the Cité-des-Hospitalières and featured three of the six authors of the book: Dave Gray-Donald, Bronwen Tucker and Angèle Alook. Along with a discussion of the perspectives offered by the book, a local panel on issues related to climate justice also took place, The local panel featured:
Waba Moko (Shannon Chief )- Anishinaabe nation, Moose Moratorium Project 
Alana Dawn Phillips - Kanien'kehà:ka nation - Concordia University graduate student
Juhi Sohani - co-founder Imagining an Otherwise, member of Climate Justice Montreal
Meredith Marty-Dugas - Office of Sustainability - Concordia University
Stefan Christoff – Media maker, Free City Radio, #MusiciansForPalestine, Immigrant Workers Centre, Cinema Politica Concordia
We recommend getting the book at your local library or through BTL books and are potentially planning a follow-up event for fall 2023!

