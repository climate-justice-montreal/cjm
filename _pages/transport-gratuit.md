---
title: "Transport en commun gratuit"
lang: fr
trans: free-public-transit
permalink: /campagnes/transport-en-commun-gratuit
---
En été 2022, Justice Climatique Montréal a lancé sa campagne pour les transports en commun gratuits en réponse immédiate à la décision du gouvernement municipal de hausser les tarifs. Cette campagne fait aussi partie de notre stratégie à long terme pour mettre en œuvre des initiatives qui favorisent la justice climatique et sociale.

Le transport représente actuellement 40% des émissions de gaz à effets de serre du Québec, ce qui en fait le secteur économique le plus polluant. Bien que l’utilisation des voitures individuelles contribue de façon importante à ce taux de pollution, nous ne pouvons pas placer le blâme sur les choix personnels des gens en matière de consommation, puisque ce sont des facteurs systémiques qui jouent le plus grand rôle.

Pour de nombreuses personnes à Montréal, il semble plus pratique de posséder une voiture que de se fier au transport en commun, même si les voitures peuvent coûter des milliers de dollars par année. De plus, le coût du transport en commun est lui-même une barrière qui touche de façon disproportionnée les résident.e.s les plus pauvres de Montréal. Malgré tout, notre gouvernement provincial a décidé de concentrer ses efforts de lutte climatique en investissant dans les subventions de véhicules électriques, ce qui ne règle pas le problème des coûts liés au transport, et remplace l’extraction destructive des combustibles fossiles par l’extraction destructive des minéraux rares.

**Pour combattre les problèmes environnementaux et sociaux liés au transport individuel, JCM demande au gouvernement d’investir davantage dans l’infrastructure des transports en commun en élargissant et en améliorant le service, ainsi qu’en offrant le transport gratuit. Nous dénonçons les compressions budgétaires qui ont été faites dans le secteur du transport en commun, ainsi que la dépendance à l’égard des véhicules électriques, qui sont une fausse solution aux crises écologiques et climatiques.**

Jusqu’à présent, notre campagne a orienté ses efforts vers des discussions dans les stations de métro et des événements communautaires, ce qui a donné des résultats positifs. Nous avons utilisé notre plateforme dans les médias sociaux pour amplifier l’attention vers les enjeux et les actualités concernant les transports en commun. Nous avons aussi placé des affiches à plusieurs endroits dans la ville afin de promouvoir notre message sur l’importance d’un service de transport en commun accessible et gratuit.
Cependant, il reste encore beaucoup de chemin à faire et nous continuons de réévaluer notre approche afin de déterminer les meilleures étapes à suivre. Nous encourageons ceux qui sont intéressé.e.s par notre campagne à [communiquer avec nous](/contact-fr) et à nous aider dans notre mission.
