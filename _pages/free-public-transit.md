---
title: "Free Public Transit"
lang: en
trans: free-public-transit
permalink: /campaigns/free-public-transit
---
In the summer of 2022, Climate Justice Montreal launched our Public Transit Campaign as an immediate response to the city’s decision to raise fares, but also as part of a longer term strategy to advance climate and social justice initiatives.

Transportation currently accounts for 40% of Quebec’s greenhouse gas emissions, making it the most polluting sector of the economy. While personal vehicle use represents a major contribution to this amount, we cannot place the blame on individuals’ consumption choices when larger systemic factors are at play. 

For many people in Montreal, having a car seems more practical than relying on public transit, even as cars cost them thousands of dollars a year. In addition, the cost of public transit is itself a barrier that disproportionately burdens less wealthy Montrealers. Despite this, our provincial government chooses to focus their climate action on investing in electric vehicle subsidies, which does not really address transport costs, and replaces destructive fossil fuel extraction with destructive rare mineral extraction.

**To address both the environmental and social impacts of individualized transport, CJM is calling for greater investments into public transit infrastructure by expanding and improving the service and by making it free to use. We condemn budget cuts to public transit and an overreliance on electric vehicles as a false solution to climate and economic crises.**

Our campaign has thus far focused on tabling outside of metro stations and community events to engage with people and gauge public interest in the campaign, which has thus far been positive. We have used our social media platform to amplify issues and developments related to transit. We have put up posters at several locations to spread our message about the importance of accessible and affordable public transit.

However, there is much work to be done and we are always reassessing our approach to determine the most effective next steps. We encourage those who are interested to [reach out to us](/contact-en) to get involved and help us continue this struggle.
