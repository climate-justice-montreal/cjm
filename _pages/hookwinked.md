---
title: "Hoodwinked in the Hothouse"
lang: en
trans: hoodwinked
permalink: /campaigns/hoodwinked
---
In April 2021, the popular education zine, Hoodwinked in the Hothouse, released the third edition. Here is the intro to it:

> Welcome back!
> 
> In the 12 years since the second edition of Hoodwinked in the Hothouse was released as a pop-ed zine, practices and policies to address climate change have expanded and deepened false solutions in shocking and alarming ways.  We see the pressing need to address the root causes of environmental and climate injustices by confronting four centuries of colonial-imperialism, ongoing patriarchal and white supremacist oppression, and today’s extreme neoliberal, globalized, industrial capitalist expansion. Hoodwinked demonstrates how climate change false solutions perpetuate, expand and reinforce these structures.
> 
> Many of us have been embroiled in a climate change narrative war with big business for at least two decades. Climate policies and programs are masked inside a narrative that has very real and violent impacts on the planet.  Because false solutions are embedded in the root causes of climate change, this historical and ongoing conflict is generational, erecting a barrier that keeps us from implementing real solutions. We hope Hoodwinked can be a tool to resist the false solutions that block us from realizing meaningful, just and lasting change.
> 
> To use this zine, sections are written to stand alone so they can be read in whatever order makes sense to you at the time. We have highlighted words and phrases in bold throughout the text that are in the glossary at the very end. The website has a much more expanded glossary with additional items and longer definitions. Keep an eye on the website for more information, translations and updates.
> 
> We encourage readers, activists, teachers and allied dreamers to distribute and print at will!
> 
> *The HITH3.0 Design and Editorial Team*

Climate Justice Montreal now has a workshop crafted around the zine and focusing on issues such as hydroelectricity, carbon capture, natural gas and the real solutions to the climate crisis! If your organization, class, group or any other gathering is interested in having a workshop, along with physical copies of Hoodwinked in the Hothouse, please contact us!

