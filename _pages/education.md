---
title: Education
lang: en
gt: éducation
trans: education
permalink: /wg/education
feat_resource: HOODWINKED_ThirdEdition
---
Climate Justice Montreal hosts workshops, panel discussions, and other educational events about climate justice and resistance to fossil fuel expansion.

To learn more about our upcoming events or to get involved, [contact us](/contact-en){: rel="noopener" target="_blank"}.
