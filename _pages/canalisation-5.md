---
title: "Canalisation 5"
lang: fr
trans: line-5
permalink: /campagnes/canalisation-5
---
La canalisation 5 est un pipeline dangereux faisant partie d’un réseau d’infrastructure de pétrole parcourant l’Alberta jusqu’à Montréal, et qui maintient actuellement ses activités illégales dans les Grands Lacs.

La canalisation 5 est une « bombe à retardement » dans le cœur de l’un des plus grands écosystèmes d’eau douce au monde. Ce pipeline vieux de 70 ans a déjà eu des fuites menant à un déversement d’au moins 4 millions de litres de pétrole. Le gouverneur Whitmer du Michigan a exigé la fermeture de la canalisation 5 en raison des risques posés à l’environnement. De plus, ce pipeline constitue une violation des droits des peuples autochtones, et menace l’eau potable de 40 millions de personnes, et coûterait des milliards de dollars en cas de nettoyage d’une fuite de pétrole. Malheureusement, l’entreprise qui contrôle la canalisation 5 (soutenue par le gouvernement libéral de Justin Trudeau) refuse cette fermeture.

Un pipeline aussi dangereux ne doit pas continuer ses opérations, surtout que nous sommes en pleine crise climatique. Le gouvernement canadien ne suis pas les principes de précaution, de prévention de la pollution et de coopération intergouvernementale de la Loi canadienne sur la protection de l’environnement, et doit appuyer la décision de fermer la canalisation 5 avant qu’une fuite désastreuse ne détruise les Grands lacs en polluant l’eau de 40 millions de personnes.

Cliquez [ici](https://www.oilandwaterdontmix.org/problem){: rel="noopener" target="_blank"} pour de plus amples renseignements. Nous continuerons à prendre action!
