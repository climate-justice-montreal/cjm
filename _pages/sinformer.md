---
title: S'informer
lang: fr
gt: éducation
trans: education
permalink: /gt/sinformer
feat_resource: HOODWINKED_ThirdEdition
---
Justice Climatique Montréal organise des ateliers, des discussions en groupe et d’autres événements éducatifs sur la justice climatique et la résistance à l’expansion des énergies fossiles.

Pour en savoir plus sur nos actions à venir ou pour vous impliquer, [contactez-nous](/contact-fr).
