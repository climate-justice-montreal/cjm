---
title: "Trompés dans la serre chaude"
lang: fr
trans: hoodwinked
permalink: /campagnes/trompés-dans-la-serre-chaude
---
En avril 2021, le populaire zine Trompés dans la serre chaude a publié sa troisième édition. Voici l’introduction :

> Bienvenue et bon retour !
> 
> Dans les 12 années depuis que la deuxième édition de « Trompés dans la serre chaude » fut publiée comme un zine d’éducation populaire, les pratiques et les politiques pour aborder les changements climatiques ont pris de l’ampleur et approfondi les fausses solutions de manières choquantes et alarmantes. Nous constatons un besoin pressant d’aborder les causes fondamentales des injustices environnementales et climatiques en confrontant quatre siècles d’impérialisme colonial, d’oppression patriarcale et de suprématie blanche continuelle, ainsi que l’expansion actuelle néolibérale, mondialisée et capitaliste industrielle. « Trompés » démontre bien comment les fausses solutions aux changements climatiques perpétuent, élargissent et renforcent ces structures.  Plusieurs d’entre nous avons été mêlés dans une guerre narrative sur les changements climatiques avec les grandes sociétés (entreprises) depuis au moins deux décennies.  Les politiques et les programmes en matière de climat sont masqués à l’intérieur d’un narratif qui a des impacts très réels et violents sur la planète. Parce que de fausses solutions sont incorporées dans les causes fondamentales des changements climatiques, ce conflit historique et continu est générationnel, érigeant ainsi une barrière qui nous empêche de mettre en œuvre de vraies solutions. Nous espérons que « Trompés » puisse être un outil pour résister les fausses solutions qui nous font entrave pour réaliser un changement juste, réel et durable.
> 
> Pour utiliser ce zine, les sections sont rédigées de sorte à servir de documents autonomes afin qu’elles puissent être lues dans l’ordre qui vous fait le plus de sens à ce moment-là. Nous avons surligné, ici et là dans le texte, des mots et des phrases en caractères gras qui se retrouvent dans le glossaire à la toute fin. Le site web contient un glossaire bien plus vaste avec des articles additionnels et des définitions plus longues. Vérifiez le site web régulièrement pour plus de renseignements, des traductions et des mise à jour.  Nous encourageons les lecteurs, les militants, les enseignants et les rêveurs alliés de distribuer et d’imprimer à volonté. Alors, tout le monde, c’est le temps de retirer nos têtes du sable !
> 
> Équipe éditoriale et de conception de TDLSC 3.0
> Avril 2021

Justice Climatique Montréal à maintenant un atelier conçu autour du zine et se concentrant sur des questions telles que l'hydroélectricité, la capture du carbone, le gaz naturel et les vraies solutions à la crise climatique! Si votre organisation, classe, groupe ou tout autre rassemblement est intéressé à avoir un atelier, ainsi que des copies physiques de Trompés dans la serre chaude, veuillez [nous contacter](/contact-fr) !
