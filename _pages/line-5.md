---
title: "Line 5"
lang: en
trans: line-5
permalink: /campaigns/line-5
feat_resource: line-5-media-backgrounder_v3
---

Line 5 is a dangerous pipeline that is part of a network of oil infrastructure that runs from Alberta to Montreal and is currently operating illegally in the Great Lakes.

Line 5 is a “ticking time bomb” in the heart of one of the largest freshwater ecosystems in the world. This 70-year-old pipeline has already spilled more than 4 million liters of oil. Michigan’s Governor Whitmer ordered a Line 5 shut down in 2020 due to the risks it poses to the environment. In addition, this pipeline violates Indigenous rights, puts drinking water at risk for 40 million people, and would cost billions of dollars in clean up in the event of a spill. However, the company that operates it (and which is supported by the Liberal government of Justin Trudeau) refuses to close it.

Such a dangerous pipeline cannot continue to operate, especially since we are in the midst of a climate crisis. The Canadian government does not follow the principles of precaution, pollution prevention and intergovernmental cooperation of the Canadian Environmental Protection Act and should support the order to close Line 5 before a disastrous spill destroys the Great Lakes and pollutes the drinking water of 40 million people.

More information [here](https://www.oilandwaterdontmix.org/problem){: rel="noopener" target="_blank"} and more actions to come!
